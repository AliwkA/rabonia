import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import Colors from '../../../constants/Colors'

import SimpleBtn from './SimpleBtn'


type Props = {

}


let _2_ButtonsCard = (props: Props) =>
(
    <View style={ styles.container }>

        <SimpleBtn
            icon = { require('../../../../assets/images/settingPageIcons/notifications.png') }
            title = 'Notifications'
            infoText = 'On'
            onPress = { () => {} }
        />

        <SimpleBtn
            icon = { require('../../../../assets/images/settingPageIcons/privacy.png') }
            title = 'Privacy'
            onPress = { () => {} }
        />
    </View>
)
export default _2_ButtonsCard
_2_ButtonsCard.defaultProps = {

}



const styles = StyleSheet.create({
    container: {
        marginTop: 30,
        paddingBottom: 15,
        paddingHorizontal: 20,
        backgroundColor: Colors.d_PageBackground,
        borderRadius: 10,

        // Shadow
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
})
