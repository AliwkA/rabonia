import React, { version } from 'react'
import { View, Text, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'
import packageJson from '../../../package.json'


type Props = {

}


let VersionView = (props: Props) =>
(
    <View style={ styles.container }>
        <Text style={ styles.versionText }>{ `${packageJson.name} v${packageJson.version}` }</Text>
    </View>
)
export default VersionView
VersionView.defaultProps = {

}



const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        alignItems: 'center',
    },
    versionText: {
        color: Colors._9e9e9e_Text,
    },
})
