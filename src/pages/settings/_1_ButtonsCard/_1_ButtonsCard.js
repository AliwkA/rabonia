import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import Colors from '../../../constants/Colors'

import SimpleBtn from './SimpleBtn'


type Props = {

}


let _1_ButtonsCard = (props: Props) =>
(
    <View style={ styles.container }>

        <SimpleBtn
            icon = { require('../../../../assets/images/settingPageIcons/availability.png') }
            title = 'Availability'
            infoText = 'On'
            onPress = { () => {} }
        />

        <SimpleBtn
            icon = { require('../../../../assets/images/settingPageIcons/username.png') }
            title = 'Username'
            infoText = '@Ali.Kerimzadeh'
            onPress = { () => {} }
        />

        <SimpleBtn
            icon = { require('../../../../assets/images/settingPageIcons/phone.png') }
            title = 'Phone'
            infoText = '+99455555555'
            onPress = { () => {} }
        />

    </View>
)
export default _1_ButtonsCard
_1_ButtonsCard.defaultProps = {

}



const styles = StyleSheet.create({
    container: {
        paddingBottom: 15,
        paddingHorizontal: 20,
        backgroundColor: Colors.d_PageBackground,
        borderRadius: 10,

        // Shadow
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
})
