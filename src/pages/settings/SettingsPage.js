import React, { Component } from 'react'
import { ScrollView, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'

import PageViewer from '../../components/pageViewers/MainPageViewer'
import HeaderView from './HeaderView'
import _1_ButtonsCard from './_1_ButtonsCard/_1_ButtonsCard'
import _2_ButtonsCard from './_2_ButtonsCard/_2_ButtonsCard'
import _3_ButtonsCard from './_3_ButtonsCard/_3_ButtonsCard'
import VersionView from './VersionView'


type Props = {
    // navigation: StackNavigationProp<''>,
}


export default class SettingsPage extends Component<Props>
{
    state: {

    }

    constructor(props)
    {
        super(props)

        this.state = {

        }
    }


    render()
    {
        return (
            <PageViewer
                safeAreaColor = { Colors.orange_Header }
                backgroundColor = { Colors.light_grey_PageBackground }
            >

                <HeaderView
                    title = 'Settings'
                />

                <ScrollView
                    contentContainerStyle = { styles.scrollContainer }
                    showsVerticalScrollIndicator = { false }
                >
                    <_1_ButtonsCard/>

                    <_2_ButtonsCard/>

                    <_3_ButtonsCard/>

                    <VersionView/>

                </ScrollView>

            </PageViewer>
        )
    }

}



const styles = StyleSheet.create({
    scrollContainer: {
        padding: 20,
        paddingBottom: 20,
    },
})
