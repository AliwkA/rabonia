import React from 'react'
import { View, Text, Image, TouchableOpacity, StatusBar, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'


type Props = {
    onPressBack: () => void,
    title: String,
}


let HeaderView = (props: Props) =>
(
    <>
    <StatusBar
        barStyle = 'dark-content'
        backgroundColor = { Colors.orange_Header }
    />
    <View style={ styles.container }>

        <TouchableOpacity
            style = { styles.sideView }
            hitSlop = { {
                top: 20,
                left: 20,
                right: 20,
                bottom: 20,
            } }
            onPress = { props.onPressBack }
        >
            <Image
                source = { require('../../../assets/images/icons/back.png') }
            />
        </TouchableOpacity>

        <View style={ styles.centerView }>
            <Text style={ styles.titleText }>{ props.title }</Text>
        </View>

        <TouchableOpacity
            style = { styles.sideView }
            hitSlop = { {
                top: 20,
                left: 20,
                right: 20,
                bottom: 20,
            } }
            onPress = { props.onPressNotification }
        >
        </TouchableOpacity>

    </View>
    </>
)
export default HeaderView
HeaderView.defaultProps = {

}



const styles = StyleSheet.create({
    container: {
        height: 60,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.orange_Header,
    },
    sideView: {

    },
    centerView: {
        flex: 1,
        alignItems: 'center',
    },
    titleText: {
        fontSize: 16,
        textTransform: 'uppercase',
        color: Colors.white_Text,
    },
})
