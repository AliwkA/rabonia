import React from 'react'
import { View, Text, Image, Switch, TouchableOpacity, ImageSourcePropType, StyleSheet, } from 'react-native'
import Colors from '../../../constants/Colors'


type Props = {
    icon: ImageSourcePropType,
    title: String,
    infoText: String,
    value: Boolean,
    onValueChange: (value: Boolean) => void,
}


let WithSwitchBtn = (props: Props) =>
(
    <TouchableOpacity
        style = { styles.btn }
        onPress = { () => props.onValueChange && props.onValueChange(!props.value) }
    >
        <View style={ styles.leftView }>
            <Image
                source = { props.icon }
            />
            <Text style={ styles.btnTitleText }>{ props.title }</Text>
        </View>

        <View style={ styles.rightView }>
            <Text style={ styles.btnInfoText }>{ props.infoText }</Text>
            <Switch
                onValueChange = { props.onValueChange }
                value = { props.value }
            />
        </View>
    </TouchableOpacity>
)
export default WithSwitchBtn
WithSwitchBtn.defaultProps = {
    infoText: '',
}



const styles = StyleSheet.create({
    btn: {
        paddingTop: 15,
        paddingBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: Colors._dcdfe8_Border,
    },
    leftView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    rightView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    btnTitleText: {
        marginLeft: 10,
    },
    btnInfoText: {
        marginRight: 10,
        color: Colors._9e9e9e_Text,
    },
})
