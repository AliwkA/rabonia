import React from 'react'
import { View, StyleSheet, } from 'react-native'
import Colors from '../../../constants/Colors'

import SimpleBtn from './SimpleBtn'
import WithSwitchBtn from './WithSwitchBtn'


type Props = {

}


let _3_ButtonsCard = (props: Props) =>
(
    <View style={ styles.container }>

        <SimpleBtn
            icon = { require('../../../../assets/images/settingPageIcons/language.png') }
            title = 'Language'
            infoText = 'English'
            onPress = { () => {} }
        />

        <WithSwitchBtn
            icon = { require('../../../../assets/images/settingPageIcons/dark_mode.png') }
            title = 'Dark mode'
            infoText = 'Off'
            value = { false } 
            onValueChange = { value => console.log(value) }
        />

        <SimpleBtn
            icon = { require('../../../../assets/images/settingPageIcons/help.png') }
            title = 'Help'
            onPress = { () => {} }
        />
    </View>
)
export default _3_ButtonsCard
_3_ButtonsCard.defaultProps = {

}



const styles = StyleSheet.create({
    container: {
        marginTop: 30,
        paddingBottom: 15,
        paddingHorizontal: 20,
        backgroundColor: Colors.d_PageBackground,
        borderRadius: 10,

        // Shadow
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
})
