import React, { Component } from 'react'
import { View, ScrollView, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'

import PageViewer from '../../components/pageViewers/MainPageViewer'
import HeaderView from './HeaderView'
import TopCnt from './TopCnt'
import CalendarView from './CalendarView'
import CardsView from './CardsView'
import ActivityCnt from './ActivityCnt/ActivityCnt'
import PlusFabBtn from '../../components/fabButtons/PlusFabBtn'


type Props = {
    // navigation: StackNavigationProp<''>,
}


export default class TestPage extends Component<Props>
{
    state: {

    }

    constructor(props)
    {
        super(props)

        this.state = {

        }
    }


    render()
    {
        return (
            <PageViewer
                safeAreaColor = { Colors.d_PageBackground }
            >

                <HeaderView
                    onPressSearch = { () => {} }
                    onPressNotification = { () => {} }
                    badgeCount = { 3 }
                />

                <ScrollView
                    contentContainerStyle = { styles.scrollContainer }
                    showsVerticalScrollIndicator = { false }
                >

                    <TopCnt
                        fullName = 'Tom'
                        items = { fakeItems }
                    />

                    <CalendarView
                        selectedDate = { new Date() }
                        onPressDate = { () => {} }
                    />

                    <CardsView
                        onPressStatistics = { () => {} }
                        onPressTacticalBoard = { () => {} }
                        onPressDailyPractice = { () => {} }
                        onPressSkillCard = { () => {} }
                    />

                    <ActivityCnt
                        items = { fakeActivityItems }
                    />

                </ScrollView>

                <PlusFabBtn
                    onPress = { () => {} }
                />

            </PageViewer>
        )
    }

}



const styles = StyleSheet.create({
    scrollContainer: {
        paddingBottom: 20,
    },
})



const fakeItems = [
    {
        title: 'Match',
        image: require('../../../assets/images/icons/itemImage.png')
    },
    {
        title: 'Match',
        image: require('../../../assets/images/icons/itemImage2.png')
    },
    {
        title: 'Match',
        image: require('../../../assets/images/icons/itemImage.png')
    },
    {
        title: 'Match',
        image: require('../../../assets/images/icons/itemImage2.png')
    },
    {
        title: 'Match',
        image: require('../../../assets/images/icons/itemImage.png')
    },
    {
        title: 'Match',
        image: require('../../../assets/images/icons/itemImage2.png')
    },
    {
        title: 'Match',
        image: require('../../../assets/images/icons/itemImage.png')
    },
]

const fakeActivityItems = [
    {
        id: '0',
        title: 'Daily Practice',
        message: 'You passed  the 300 meter Run.',
    },
    {
        id: '1',
        title: 'Daily Practice',
        message: 'You passed  the 300 meter Run.',
    },
    {
        id: '2',
        title: 'Daily Practice',
        message: 'You passed  the 300 meter Run.',
    },
    {
        id: '3',
        title: 'Daily Practice',
        message: 'You passed  the 300 meter Run.',
    },
    {
        id: '4',
        title: 'Daily Practice',
        message: 'You passed  the 300 meter Run.',
    },
    {
        id: '5',
        title: 'Daily Practice',
        message: 'You passed  the 300 meter Run.',
    },
    {
        id: '6',
        title: 'Daily Practice',
        message: 'You passed  the 300 meter Run.',
    },
]