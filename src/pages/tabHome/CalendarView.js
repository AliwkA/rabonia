import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'


type Props = {
    selectedDate: Date,
    onPressDate: (date: Date) => void,
    dayNames: String[],
    monthNames: String[],
}

const days = [0, 1, 2, 3, 4, 5, 6]

let CalendarView = (props: Props) =>
{
    let dayIndex = props.selectedDate.getDay()
    let startDate = new Date(props.selectedDate)
    startDate.setDate(props.selectedDate.getDate() - dayIndex)

    return (
        <View style={ styles.container }>
            <View style={ styles.cnt }>
    
                <View style={ styles.headerView }>
    
                    <View style={ styles.headerLeftView }>
                        <Text style={ styles.dateText }>{ `${props.selectedDate.getDate()} ${props.monthNames[props.selectedDate.getMonth()]}` }</Text>
                    </View>
    
                    <View style={ styles.headerRightView }>
                        <Image
                            source = { require('../../../assets/images/icons/calendar_home.png') }
                        />
                    </View>
    
                </View>
    
                <View style={ styles.bodyView }>
    
                    <View style={ styles.dayNamesView }>
                        {
                            days.map(dayIndex => (
                                <TouchableOpacity
                                    key = { dayIndex.toString() }
                                    style = { styles.dayNameCellBtn }
                                    disabled = { true }
                                >
                                    <Text
                                        style = { styles.dayNameCellText }
                                        numberOfLines = { 1 }
                                    >{ props.dayNames[dayIndex] }</Text>
                                </TouchableOpacity>
                            ))
                        }
                    </View>
    
                    <View style={ styles.daysView }>
                        {
                            days.map(dayIndex => {
                                const date = new Date(startDate)
                                date.setDate(startDate.getDate() + dayIndex)
                                return (
                                    <TouchableOpacity
                                        key = { dayIndex.toString() }
                                        style = { styles.dayNameCellBtn }
                                        onPress = { () => props.onPressDate && props.onPressDate(date) }
                                    >
                                        <Text
                                            style = { date.getDate() == props.selectedDate.getDate() && styles.selectedDayText }
                                            numberOfLines = { 1 }
                                        >{ date.getDate() }</Text>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </View>
    
                </View>
    
            </View>
        </View>
    )
}
export default CalendarView
CalendarView.defaultProps = {
    selectedDate: new Date(),
    dayNames: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    monthNames: ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ],
}



const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        paddingHorizontal: 20,
    },
    cnt: {
        backgroundColor: Colors.d_PageBackground,
        borderRadius: 5,

        // Shadow
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    headerView: {
        paddingVertical: 12,
        paddingHorizontal: 20,
        flexDirection: 'row',
        borderColor: Colors.d_Border,
        borderBottomWidth: 1,
    },
    headerLeftView: {
        flex: 1,
    },
    headerRightView: {

    },
    dateText: {
        color: Colors.grey_Text,
    },
    bodyView: {

    },
    dayNamesView: {
        flexDirection: 'row',
    },
    daysView: {
        flexDirection: 'row',
    },
    dayNameCellBtn: {
        padding: 11,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    dayNameCellText: {
        color: Colors.grey_Text,
    },
    selectedDayText: {
        margin: -5,
        padding: 5,
        color: Colors.white_Text,
        backgroundColor: Colors.d_Orange,
        borderRadius: 5,
        overflow: 'hidden',
    },
})
