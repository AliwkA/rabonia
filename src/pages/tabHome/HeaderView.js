import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'


type Props = {
    badgeCount: Number,
    onPressSearch: () => void,
    onPressNotification: () => void,
}


let HeaderView = (props: Props) =>
(
    <View style={ styles.container }>

        <TouchableOpacity
            style = { styles.sideView }
            hitSlop = { {
                top: 20,
                left: 20,
                right: 20,
                bottom: 20,
            } }
            onPress = { props.onPressSearch }
        >
            <Image
                source = { require('../../../assets/images/icons/magnifier.png') }
            />
        </TouchableOpacity>

        <View style={ styles.centerView }>
            <Image
                source = { require('../../../assets/images/icons/header_logo.png') }
            />
        </View>

        <TouchableOpacity
            style = { styles.sideView }
            hitSlop = { {
                top: 20,
                left: 20,
                right: 20,
                bottom: 20,
            } }
            onPress = { props.onPressNotification }
        >
            <Image
                source = { require('../../../assets/images/icons/bell.png') }
            />
            {
                props.badgeCount
                &&
                <View style={ styles.badgeView }>
                    <Text style={ styles.badgeText } numberOfLines={ 1 }>{ props.badgeCount }</Text>
                </View>
            }
        </TouchableOpacity>

    </View>
)
export default HeaderView
HeaderView.defaultProps = {
    badgeCount: null,
}



const styles = StyleSheet.create({
    container: {
        height: 60,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.d_PageBackground,
    },
    sideView: {

    },
    centerView: {
        flex: 1,
        alignItems: 'center',
    },
    badgeView: {
        position: 'absolute',
        padding: 3,
        top: -10,
        right: -10,
        aspectRatio: 1,
        backgroundColor: Colors.d_Orange,
        borderRadius: 10,
    },
    badgeText: {
        fontSize: 12,
        color: Colors.white_Text,
    },
})
