import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'


type Props = {
    onPressStatistics: () => void,
    onPressTacticalBoard: () => void,
    onPressDailyPractice: () => void,
    onPressSkillCard: () => void,
}


let CardsView = (props: Props) =>
(
    <View style={ styles.container }>

        <TouchableOpacity
            style = { styles.btn }
            onPress = { props.onPressStatistics }
        >
            <View style={ styles.cardView }>
                <Image
                    style = { styles.img }
                    source = { require('../../../assets/images/icons/tabhome_crad_1.png') }
                />
            </View>
            <Text style={ styles.titleText }>Title</Text>
        </TouchableOpacity>

        <TouchableOpacity
            style = { styles.btn }
            onPress = { props.onPressTacticalBoard }
        >
            <View style={ styles.cardView }>
                <Image
                    style = { styles.img }
                    source = { require('../../../assets/images/icons/tabhome_crad_2.png') }
                />
            </View>
            <Text style={ styles.titleText }>Title</Text>
        </TouchableOpacity>

        <TouchableOpacity
            style = { styles.btn }
            onPress = { props.onPressDailyPractice }
        >
            <View style={ styles.cardView }>
                <Image
                    style = { styles.img }
                    source = { require('../../../assets/images/icons/tabhome_crad_3.png') }
                />
            </View>
            <Text style={ styles.titleText }>Title</Text>
        </TouchableOpacity>

        <TouchableOpacity
            style = { styles.btn }
            onPress = { props.onPressSkillCard }
        >
            <View style={ styles.cardView }>
                <Image
                    style = { styles.img }
                    source = { require('../../../assets/images/icons/tabhome_crad_4.png') }
                />
            </View>
            <Text style={ styles.titleText }>Title</Text>
        </TouchableOpacity>

    </View>
)
export default CardsView



const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    btn: {
        width: '21%',
    },
    cardView: {
        padding: 30,
        aspectRatio: 1,
        backgroundColor: Colors.d_PageBackground,
        alignItems: 'center',
        justifyContent: 'center',

        // Shadow
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    titleText: {
        marginTop: 10,
        textAlign: 'center',
    },
})
