import React from 'react'
import { View, Text, Image, ImageSourcePropType, TouchableOpacity, FlatList, StyleSheet, } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import Colors from '../../constants/Colors'


type Props = {
    fullName: String,
    items: [{ title: String, image: ImageSourcePropType }]
}


const TopCnt = (props: Props) =>
(
    <View style={ styles.container }>

        <LinearGradient
            style={ styles.background }
            colors = { [ '#FE8500', '#FE6819' ] }
        />

        <View style={ styles.topView }>
            <View style={ styles.leftView }>
                <Text style={ styles.topText }>Hello, { props.fullName }</Text>
                <Text style={ styles.bottomText }>Let’s go play</Text>
            </View>

            <View style={ styles.rightView }>

                <Image
                    source = { require('../../../assets/images/icons/profile_circle.png') }
                />

            </View>
        </View>

        <View style={ styles.itemsView }>
            <FlatList
                contentContainerStyle = { styles.flatListCnt }
                horizontal = { true }
                showsHorizontalScrollIndicator = { false }
                decelerationRate = 'fast'
                data = { props.items }
                keyExtractor = { (item, i) => i.toString() }
                renderItem = { ({ item }) => (
                    <TouchableOpacity
                        style = { styles.itemBtn }
                    >
                        <Image
                            source = { item.image }
                        />
                        <Text style={ styles.itemTitleText }>{ item.title }</Text>
                    </TouchableOpacity>
                ) }
            />

        </View>
        


    </View>
)
export default TopCnt
TopCnt.defaultProps = {
    badgeCount: null,
}



const styles = StyleSheet.create({
    container: {

    },
    background: {
        height: 220,
        position: 'absolute',
        backgroundColor: Colors.d_Orange,
        left: 0,
        right: 0,
    },
    topView: {
        flexDirection: 'row',
        paddingVertical: 30,
        paddingHorizontal: 20,
        alignItems: 'center',
    },
    leftView: {
        flex: 1,
    },
    rightView: {

    },
    topText: {
        fontSize: 18,
        color: Colors.light_Orang_Text,
    },
    bottomText: {
        marginTop: 10,
        fontSize: 22,
        color: Colors.white_Text,
    },
    itemsView: {

    },
    flatListCnt: {
        paddingRight: 20,
        paddingVertical: 10,
    },
    itemBtn: {
        marginLeft: 20,
        paddingTop: 40,
        paddingBottom: 10,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.d_PageBackground,
        borderRadius: 10,

        // Shadow
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    itemTitleText: {
        paddingVertical: 10,
    },
})
