import React from 'react'
import { View, Text, FlatList, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import { ActivityItemMobM } from '../../../models/mobileModels'
import Colors from '../../../constants/Colors'

import ButtonWithGradient from '../../../components/buttons/ButtonWithGradient'
import ActivityItem from './ActivityItem'


type Props = {
    items: ActivityItemMobM[],
}


let ActivityCnt = (props: Props) =>
(
    <View style={ styles.container }>

        <View style={ styles.headerView }>

            <Text style={ styles.titleText }>Activity</Text>

            <ButtonWithGradient
                style = { styles.seeAllBtn }
                colors = { [ '#FE8500', '#FE6819' ] }
            >
                <Text style={ styles.seeAllBtnText }>See All</Text>
            </ButtonWithGradient>

        </View>

        {
            props.items.map(item => (
                <ActivityItem
                    key = { item.id }
                    item = { item }
                />
            ))
        }

    </View>
)
export default ActivityCnt
ActivityCnt.defaultProps = {
    items: [],
}



const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        paddingHorizontal: 20,
    },
    headerView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    titleText: {
        fontSize: 22,
        fontWeight: 'bold',
        color: Colors._1d1d1d_Text,
    },
    seeAllBtn: {
        padding: 10,
        borderRadius: 50,
    },
    seeAllBtnText: {
        fontSize: 12,
        color: Colors.white_Text,
    },
})
