import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import { ActivityItemMobM } from '../../../models/mobileModels'
import Colors from '../../../constants/Colors'


type Props = {
    item: ActivityItemMobM,
}


let ActivityItem = (props: Props) =>
(
    <View style={ styles.container }>

        <View style={ styles.imgView }>
            <Image
                source = { require('../../../../assets/images/icons/activity_item_icon.png') }
            />
        </View>

        <View style={ styles.rightView }>
            <Text style={ styles.titleText }>{ props.item.title }</Text>
            <Text style={ styles.messageText }>{ props.item.message }</Text>
        </View>

    </View>
)
export default ActivityItem
ActivityItem.defaultProps = {

}



const styles = StyleSheet.create({
    container: {
        marginTop: 15,
        flexDirection: 'row',
        alignItems: 'center',
    },
    imgView: {
        padding: 12,
        backgroundColor: Colors.d_PageBackground,
        aspectRatio: 1,
        borderRadius: 50,

        // Shadow
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    rightView: {
        marginLeft: 10,
        flex: 1,
    },
    titleText: {
        color: Colors._1d1d1d_Text,
    },
    messageText: {
        marginTop: 2,
        color: Colors._c4c4c4_Text,
    },
})
