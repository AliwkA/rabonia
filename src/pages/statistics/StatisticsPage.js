import React, { Component } from 'react'
import { ScrollView, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'

import PageViewer from '../../components/pageViewers/MainPageViewer'
import WhiteBackHeader from '../../components/headers/WhiteBackHeader'
import TopTabNav from './topTabNav/TopTabNav'


type Props = {
    // navigation: StackNavigationProp<''>,
}


export default class SettingsPage extends Component<Props>
{
    state: {

    }

    constructor(props)
    {
        super(props)

        this.state = {

        }
    }


    render()
    {
        return (
            <PageViewer
                safeAreaColor = { Colors.white_Header }
            >

                <WhiteBackHeader
                    onPressBack = { () => {} }
                />

                <ScrollView
                    contentContainerStyle = { styles.scrollContainer }
                    showsVerticalScrollIndicator = { false }
                >
                    <TopTabNav/>
                </ScrollView>

            </PageViewer>
        )
    }

}



const styles = StyleSheet.create({
    scrollContainer: {
        padding: 20,
    },
})
