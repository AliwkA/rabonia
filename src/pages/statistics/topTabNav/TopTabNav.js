import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import Animated from 'react-native-reanimated'
import Colors from '../../../constants/Colors'

import PlayerTabPage from './PlayerTabPage'


type Props = {

}

const Tab = createMaterialTopTabNavigator()


let TopTabNav = (props: Props) =>
(
    <View style={ styles.container }>
        <Tab.Navigator
            style = { { backgroundColor: 'red' } }
            // tabBar = { props => <MyTabBar { ...props } /> }
        >
            <Tab.Screen name="Player" component={ PlayerTabPage }/>
            <Tab.Screen name="Team" component={ PlayerTabPage }/>
        </Tab.Navigator>
    </View>
)
export default TopTabNav
TopTabNav.defaultProps = {

}



const styles = StyleSheet.create({
    container: {

    },
})



function MyTabBar({ state, descriptors, navigation, position })
{
    return (
        <View style={{ flexDirection: 'row' }}>
            {state.routes.map((route, index) => {
            const { options } = descriptors[route.key];
            const label =
                options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                ? options.title
                : route.name;
    
            const isFocused = state.index === index;
    
            const onPress = () => {
                const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
                });
    
                if (!isFocused && !event.defaultPrevented) {
                navigation.navigate(route.name);
                }
            };
    
            const onLongPress = () => {
                navigation.emit({
                type: 'tabLongPress',
                target: route.key,
                });
            };
    
            const inputRange = state.routes.map((_, i) => i);
            const opacity = Animated.interpolate(position, {
                inputRange,
                outputRange: inputRange.map(i => (i === index ? 1 : 0)),
            });
    
            return (
                <TouchableOpacity
                accessibilityRole="button"
                accessibilityStates={isFocused ? ['selected'] : []}
                accessibilityLabel={options.tabBarAccessibilityLabel}
                testID={options.tabBarTestID}
                onPress={onPress}
                onLongPress={onLongPress}
                style={{ flex: 1 }}
                >
                <Animated.Text style={{ opacity }}>
                    {label}
                </Animated.Text>
                </TouchableOpacity>
            )
            })}
        </View>
    )
}