import React from 'react'
import { View, StyleSheet, } from 'react-native'
import Colors from '../../../constants/Colors'

import StatisticsCard from './statisticsCard/StatisticsCard'


type Props = {

}


let PlayerTabPage = (props: Props) =>
(
    <View style={ styles.container }>

        <StatisticsCard
            title = 'Player'
        />

    </View>
)
export default PlayerTabPage
PlayerTabPage.defaultProps = {

}



const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        paddingBottom: 20,
        backgroundColor: Colors.d_PageBackground,
    },
})
