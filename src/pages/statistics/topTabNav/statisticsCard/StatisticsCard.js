import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import Colors from '../../../../constants/Colors'

import PercentLine from './PercentLine'


type Props = {
    title: String,
}


let StatisticsCard = (props: Props) =>
(
    <View style={ styles.container }>

        <Text style={ styles.titleText }>{ props.title }</Text>

        <PercentLine
            title = 'Attack'
        />

    </View>
)
export default StatisticsCard
StatisticsCard.defaultProps = {

}



const styles = StyleSheet.create({
    container: {
        marginTop: 30,
        backgroundColor: Colors.d_PageBackground,

        // Shadow
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    titleText: {
        paddingTop: 20,
        paddingBottom: 10,
        fontSize: 16,
        textAlign: 'center',
    },
})
