import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, } from 'react-native'
import Colors from '../../../../constants/Colors'


type Props = {
    title: String,
}


let PercentLine = (props: Props) =>
(
    <View style={ styles.container }>
        <Text style={ styles.titleText }>{ props.title }</Text>

    </View>
)
export default PercentLine
PercentLine.defaultProps = {

}



const styles = StyleSheet.create({
    container: {

    },
    titleText: {
        textAlign: 'center',
    },
})

// // Shadow
// shadowColor: "#000",
// shadowOffset: {
//     width: 0,
//     height: 1,
// },
// shadowOpacity: 0.22,
// shadowRadius: 2.22,

// elevation: 3,