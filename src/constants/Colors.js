'use strict'

export default {
    d_OverSafeArea: '#397624',


    d_PageBackground: '#FFF',
    light_grey_PageBackground: '#F9FAFB',


    d_Header: '#FE6819',
    orange_Header: '#FF6300',
    white_Header: '#FFF',

    d_Border: '#DCDFE8',
    _dcdfe8_Border: '#DCDFE8',

    white_Text: '#FFF',
    light_Orang_Text: '#FFE1D1',
    grey_Text: '#737576',
    _1d1d1d_Text: '#1D1D1D',
    _c4c4c4_Text: '#C4C4C4',
    _9e9e9e_Text: '#9E9E9E',

    d_Orange: '#FF6300',
}
