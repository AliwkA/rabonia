import storageService from '../others/StorageService'
import BaseStrings from './languages/BaseStrings'
import StringsEN from './languages/StringsEN'
import StringsRU from './languages/StringsRU'


let currentStrings: BaseStrings = new BaseStrings()

function t(): BaseStrings
{
    return currentStrings
}


export default t



export let languageConfig =
{
    init: async () =>
    {
        let langKey = await storageService.getLangKey()
        currentStrings = getStringsByLangKey(langKey)
    },

    changeLanguage: (langKey: string) =>
    {
        currentStrings = getStringsByLangKey(langKey)

        storageService.saveLangKey(langKey)
    }
}


function getStringsByLangKey(langKey: String) : BaseStrings
{
    switch (langKey)
    {
        case 'az':
        case 'az-AZ':
        {
            return new BaseStrings()
        }

        case 'en':
        case 'en-US':
        {
            return new StringsEN()
        }

        case 'ru':
        case 'ru-RU':
        {
            return new StringsRU()
        }

        default:
        {
            return new BaseStrings()
        }   
    }
}