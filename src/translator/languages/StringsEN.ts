import BaseStrings from './BaseStrings'


export default class StringsRU extends BaseStrings
{
    AppName = 'Novbem'


    // --- Standart Strings --- //
    Error = 'Erro'
    Warning = 'Warning'
    Message = 'Message'
    Cancel = 'Cancel'
    Ok = 'Ok'
    Logout = 'Log out'
    Search = 'Search'


    // --- Page Names --- //
    MainPage = 'Main Page'



    // --- Strings specifically for this App --- //
    LoginFormTitle = 'Ваш\nруководитель очереди'
    PhoneNumber = 'Phone number'
    Password = 'Password'
    ForgotPassword = 'Forgot password?'
    Login = 'Log in'
    SignUpBtn = 'Sign up'
    SignUpTitle = 'Sign Up'
    SendSmsBtn = 'Send SMS'
    NextBtn = 'Next'
    SmsCodeTemplate = '0-0-0-0'
    GoBackToLogin = 'Go back to login'
    SignUpFirsInfo = 'Чтобы пройти регистрацию введите\nномер телефона'
    SignUpSecondInfo = 'Введите код отправленный вам по смс-у\n'
    ChangeNumberBtn = 'Change phone number'
    MyProfileBtn = 'My profile'
    MySubscriptionsBtn = 'Yazılmalarım'
    ChangePasswordBtn = 'Change password'
    LogOutBtn = 'Log out'
    Appointments = 'Appointments'
    Approved = 'Approved'
    NotApproved = 'Not approved'
    PendingApproval = 'Pending approval'
    ChangeDate = 'Change date'
    Refuse = 'Refuse'
    SaveBtn = 'Save'
    OldPassword = 'Old password'
    NewPassword = 'New password'
    RepeatPassword = 'Repeat password'

}