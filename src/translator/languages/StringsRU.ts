import BaseStrings from './BaseStrings'


export default class StringsRU extends BaseStrings
{
    AppName = 'Нобэм'


    // --- Standart Strings --- //
    Error = 'Ошибка'
    Warning = 'Предупреждение'
    Message = 'Сообщение'
    Cancel = 'Отмена'
    Ok = 'Ok'
    Logout = 'Выйти'
    Search = 'Поиск'


    // --- Page Names --- //
    MainPage = 'Главная страница'



    // --- Strings specifically for this App --- //
    LoginFormTitle = 'Ваш\nруководитель очереди'
    PhoneNumber = 'Ваш номер телефона'
    Password = 'Пароль'
    ForgotPassword = 'Забыли пароль?'
    Login = 'Войти'
    SignUpBtn = 'Регистрация'
    SignUpTitle = 'Регистрация'
    SendSmsBtn = 'отправить SMS'
    NextBtn = 'Вперед'
    SmsCodeTemplate = '0-0-0-0'
    GoBackToLogin = 'Назад ко входу'
    SignUpFirsInfo = 'Чтобы пройти регистрацию введите\nномер телефона'
    SignUpSecondInfo = 'Введите код отправленный вам по смс-у\n'
    ChangeNumberBtn = 'Поменять номер'
    MyProfileBtn = 'Мой профиль'
    MySubscriptionsBtn = 'Yazılmalarım'
    ChangePasswordBtn = 'Изменить пароль'
    LogOutBtn = 'Выйти'
    Appointments = 'Приемы'
    Approved = 'Подтвержден'
    NotApproved = 'Не подтвержден'
    PendingApproval = 'Ждет подтвержден'
    ChangeDate = 'Изменить время'
    Refuse = 'Отказ'
    SaveBtn = 'Сохранить'
    OldPassword = 'Старый пароль'
    NewPassword = 'Новый пароль'
    RepeatPassword = 'Повторите пароль'

}