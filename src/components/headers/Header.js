import React from 'react'
import { View, Text, StatusBar, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'


type Props = {
    title: String,  // Default 'Unknown'
}


export default Header = (props: Props) => {


    return (
        <>
        <View style={ styles.container }>

            <View
                style = { styles.sideView }
            >
            </View>

            <View style={ styles.headerCenter }>

                <Text style={ styles.titleText }>{ props.title }</Text>

            </View>

            <View
                style = { styles.sideView }
            >
            </View>

        </View>
        <StatusBar
            barStyle = 'light-content'
            backgroundColor = { Colors.d_Header }
        />
        </>
    )
}
Header.defaultProps = {
    title: 'Unknown',
}




const styles = StyleSheet.create({
    container: {
        height: 60,
        paddingHorizontal: 10,
        flexDirection: 'row',
        backgroundColor: Colors.d_Header,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    sideView: {
        height: 50,
        width: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerCenter: {
    },
    titleText: {
        fontSize: 20,
        color: Colors.white,
    },
})