import React from 'react'
import { View, Image, TouchableOpacity, StatusBar, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'


type Props = {
    onPressBack: () => void,
}


let WhiteBackHeader = (props: Props) =>
(
    <>
    <StatusBar
        barStyle = 'dark-content'
        backgroundColor = { Colors.white_Header }
    />
    <View style={ styles.container }>

        <TouchableOpacity
            style = { styles.sideView }
            hitSlop = { {
                top: 20,
                left: 20,
                right: 20,
                bottom: 20,
            } }
            onPress = { props.onPressBack }
        >
            <Image
                source = { require('../../../assets/images/icons/orange_back.png') }
            />
        </TouchableOpacity>

        <View style={ styles.centerView }>
            <Image
                source = { require('../../../assets/images/icons/header_logo.png') }
            />
        </View>

        <TouchableOpacity
            style = { styles.sideView }
            hitSlop = { {
                top: 20,
                left: 20,
                right: 20,
                bottom: 20,
            } }
            onPress = { props.onPressNotification }
        >
        </TouchableOpacity>

    </View>
    </>
)
export default WhiteBackHeader
WhiteBackHeader.defaultProps = {

}



const styles = StyleSheet.create({
    container: {
        height: 60,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.white_Header,
    },
    sideView: {

    },
    centerView: {
        flex: 1,
        alignItems: 'center',
    },
    titleText: {
        fontSize: 16,
        textTransform: 'uppercase',
        color: Colors.white_Text,
    },
})
