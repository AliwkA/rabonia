import React from 'react'
import { View, Text, Image, TouchableOpacity, StatusBar, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'


type Props = {
    title: String,  // Default 'Unknown'
    onBack: () => void,
}


export default BackHeader = (props: Props) => {


    return (
        <>
        <View style={ styles.container }>

            <TouchableOpacity
                style = { styles.sideView }
                onPress = { props.onBack }
            >
                <Image
                    style = { styles.icon }
                    source = { require('../../../assets/images/icons/left_arrow_header_32x32.png') }
                    resizeMode = 'contain'
                />
            </TouchableOpacity>

            <View style={ styles.headerCenter }>

                <Text style={ styles.titleText }>{ props.title }</Text>

            </View>

            <View
                style = { styles.sideView }
            >
            </View>

        </View>
        <StatusBar
            barStyle = 'light-content'
            backgroundColor = { Colors.d_Header }
        />
        </>
    )
}
// export default React.memo(BackHeader)
BackHeader.defaultProps = {
    title: 'Unknown',
}



const styles = StyleSheet.create({
    container: {
        height: 60,
        paddingHorizontal: 10,
        flexDirection: 'row',
        backgroundColor: Colors.d_Header,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    sideView: {
        height: 50,
        width: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerCenter: {
    },
    titleText: {
        fontSize: 16,
        color: Colors.white,
    },
    icon: { 
        height: 18,
    },
})