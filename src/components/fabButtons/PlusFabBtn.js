import React from 'react'
import { Image, TouchableOpacity, StyleSheet, } from 'react-native'


type Props = {
    onPress: () => void,
}


let PlusFabBtn = (props: Props) =>
(
    <TouchableOpacity
        style = { styles.container }
        onPress = { props.onPress }
    >
        <Image
            source = { require('../../../assets/images/icons/plus_fab.png') }
        />
    </TouchableOpacity>
)
export default PlusFabBtn
PlusFabBtn.defaultProps = {

}



const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        right: 25,
        bottom: 50 + 30, // tabNav + defaultBottom
    },
})
