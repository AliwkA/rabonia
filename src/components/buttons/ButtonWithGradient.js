import React from 'react'
import { TouchableOpacity, ViewStyle, Insets, StyleSheet, } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'


type Props = {
    style: ViewStyle,
    colors: String[],
    onPress: () => void,
    hitSlop: Insets,
}


let ButtonWithGradient = (props: Props) =>
(
    <TouchableOpacity
        onPress = { props.onPress }
        hitSlop = { props.hitSlop }
    >
        <LinearGradient
            style = { [styles.gradient, props.style ] }
            colors = { props.colors }
        >
            { props.children }
        </LinearGradient>
    </TouchableOpacity>
)
export default ButtonWithGradient



const styles = StyleSheet.create({
    gradient: {
        flex: 1,
    },
})
