import React from 'react'
import { Modal, ModalProps, KeyboardAvoidingView, Platform, StyleSheet, } from 'react-native'



export default class CustomModal extends React.Component<ModalProps> {

    render() {
        return (
            <Modal
                {...this.props}
            >

                <KeyboardAvoidingView
                    style = { styles.container }
                    behavior = { Platform.OS == 'ios' ? 'padding' : null }
                >
                    { this.props.children }
                </KeyboardAvoidingView>

            </Modal>
        )
    }

}




const styles = StyleSheet.create({
    container : {
        flex : 1,
    },
})
