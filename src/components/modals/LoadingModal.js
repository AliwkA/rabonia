import React, { PureComponent } from 'react'
import { View, ActivityIndicator, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'

import CustomModal from './CustomModal'


export interface ILoadingModal {
    show: (callback?: () => void) => void;
    close: (callback?: () => void) => void;
    getIsShow: () => Boolean;
}



export default class LoadingModal extends PureComponent
{
    state: {
        isShow: Boolean,
    }

    constructor(props) {
        super(props)
        
        this.state = {
            isShow: false,
        }
    }


    render()
    {
        if(!this.state.isShow)
            return null

        return (
            <CustomModal
                visible = { this.state.isShow }
                onRequestClose = { this.Close }
                transparent = { true }
                animationType = 'fade'
            >
                <View style={ styles.container }>

                    <ActivityIndicator
                        size = 'large'
                        color = { Colors.d_Green }
                    />

                </View>
            </CustomModal>
        )
    }


    show(callback = () => {})
    {
        this.setState({ isShow: true }, () => setTimeout(callback, 10))
    }

    close(callback = () => {})
    {
        this.setState({ isShow: false }, () => setTimeout(callback, 10))
    }

    getIsShow()
    {
        return this.state.isShow
    }

}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00000080',
    },
})
