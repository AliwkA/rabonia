import React from 'react'
import { Text, View, FlatList, TouchableOpacity, StyleSheet, } from 'react-native'
import Colors from '../../constants/Colors'

import CustomModal from './CustomModal'


type Props = {
    isShow: Boolean,
    items: Item[],
    onItem: (item: Item) => void,
    onRequestClose: () => void,
}

type Item = {
    title: String,
    value: AnalyserNode,
}


export default SelectBoxModal = (props: Props) =>
(
    <CustomModal
        visible = { props.isShow }
        onRequestClose = { props.onRequestClose }
        transparent = { true }
        animationType = 'fade'
    >
        <View
            style = { styles.container }
            onTouchEnd = { props.onRequestClose }
        >
            <View
                style = { styles.modalCtn }
                onTouchEnd = { e => e.stopPropagation() }
            >
                <FlatList
                    data = { props.items }
                    keyExtractor = { (item, i) => item.value.toString() }
                    renderItem = { ({ item }) =>
                        <TouchableOpacity
                            style = { styles.itemView }
                            onPress = { () => props.onItem && props.onItem(item) }
                        >
                            <Text style={ styles.itemText }>{ item.title }</Text>
                        </TouchableOpacity>
                    }
                />
            </View>
        </View>
    </CustomModal>
)




const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00000080',
    },
    modalCtn: {
        maxHeight: '80%',
        width: '70%',
        backgroundColor: Colors.d_White,
        borderRadius: 5,
    },
    itemView: {
        padding: 15,
        borderBottomWidth: 0.5,
    },
    itemText: {

    },
})
